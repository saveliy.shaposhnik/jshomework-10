const eyeBtns = document.getElementsByClassName("fas"),
  passInp = document.getElementsByClassName("pass-inp"),
  btn = document.getElementById("btn"),
  wrongPass = document.createElement("p");

for (let index = 0; index < eyeBtns.length; index++) {
  eyeBtns[index].addEventListener("click", function () {
    if (eyeBtns[index].className === "fas fa-eye icon-password") {
      passInp[index].type = "text";
      eyeBtns[index].className = "fas fa-eye fa-eye-slash icon-password";
    } else {
      passInp[index].type = "password";
      eyeBtns[index].className = "fas fa-eye icon-password";
    }
  });
}

btn.addEventListener("click",function(){
  if(passInp[0].value === passInp[1].value){
    wrongPass.remove();
    alert("You are welcome!");
    passInp[1].value = "";
    passInp[0].value = "";
  }else{
    wrongPass.textContent = "Нужно ввести одинаковые значения";
    wrongPass.style.cssText = "margin-top:0px;color:red;"
    passInp[1].after(wrongPass);
  }
});


